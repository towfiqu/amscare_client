export const url = process.env.REACT_APP_CLOUDINARY_URL;
export const preset_patient = process.env.REACT_APP_PATIENT_PRESET;
export const preset_report = process.env.REACT_APP_REPORT_PRESET;
export const formData = new FormData();
export const stripe_key = process.env.REACT_APP_STRIPE_KEY;
export const map_api = process.env.REACT_APP_GOOGLE_MAPS;

export const getApiString = () => {
  let api;

  if (process.env.NODE_ENV === 'development') {
    api = 'http://localhost:8080';
    return api;
  } else {
    api =
      'https://cors-anywhere.herokuapp.com/https://fathomless-headland-20145.herokuapp.com';
    return api;
  }
};
