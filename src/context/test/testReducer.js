import {
  GET_ALL_TESTS,
  GET_TESTS_BY_NAME,
  GET_PAGE_NUMBERS,
  SET_PAGE
} from './test_types';

export default (state, action) => {
  switch (action.type) {
    default:
      return state;

    case GET_ALL_TESTS:
      return {
        ...state,
        tests: action.payload
      };
    case GET_TESTS_BY_NAME:
      return {
        ...state,
        tests: action.payload
      };
    case GET_PAGE_NUMBERS:
      return {
        ...state,
        page_numbers: action.payload
      };
    case SET_PAGE:
      return {
        ...state,
        page: action.payload
      };
  }
};
