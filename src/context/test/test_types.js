export const GET_ALL_TESTS = 'GET_ALL_TESTS';
export const GET_TESTS_BY_NAME = 'GET_TESTS_BY_NAME';
export const GET_PAGE_NUMBERS = 'GET_PAGE_NUMBERS';
export const SET_PAGE = 'SET_PAGE';
