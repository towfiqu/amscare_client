import React, { useReducer } from 'react';
import TestContext from './testContext';
import TestReducer from './testReducer';

import axios from 'axios';

import {
  GET_ALL_TESTS,
  GET_TESTS_BY_NAME,
  GET_PAGE_NUMBERS,
  SET_PAGE,
} from './test_types';

import { getApiString } from '../../config';
const api_string = getApiString();

const TestState = props => {
  const initialState = {
    tests: [],
    page_numbers: [],
    page: 1,
  };

  const [state, dispatch] = useReducer(TestReducer, initialState);

  const getAllTests = async () => {
    try {
      const res = await axios.get(`${api_string}/api/medical_tests/getAll`);
      dispatch({
        type: GET_ALL_TESTS,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };

  const getTestsByName = async name => {
    try {
      const res = await axios.get(
        `${api_string}/api/medical_tests/get_tests_by_name/${name}`,
      );
      dispatch({
        type: GET_TESTS_BY_NAME,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };

  const getPageNumbers = () => {
    const total = state.tests.length;
    let divider = total / 3;
    divider = Math.ceil(divider);

    const pageNumber = [];

    for (let i = 1; i <= divider; i++) {
      pageNumber.push(i);
    }

    dispatch({
      type: GET_PAGE_NUMBERS,
      payload: pageNumber,
    });
  };

  const setPage = number => {
    dispatch({
      type: SET_PAGE,
      payload: number,
    });
  };

  return (
    <TestContext.Provider
      value={{
        tests: state.tests,
        page_numbers: state.page_numbers,
        page: state.page,
        getPageNumbers,
        getTestsByName,
        getAllTests,
        setPage,
      }}
    >
      {props.children}
    </TestContext.Provider>
  );
};

export default TestState;
