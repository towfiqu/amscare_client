import React, { useReducer } from 'react';
import CabinContext from './cabinContext';
import CabinReducer from './cabinReducer';

import axios from 'axios';

import {
  ADD_BOOKING,
  GET_CABINS_BY_FACILITY,
  GET_SINGLE_CABIN,
} from './cabin_types';

import { getApiString } from '../../config';
const api_string = getApiString();

const CabinState = props => {
  const initialState = {
    booking_data: {},
    cabins: [],
    cabin: null,
  };

  const [state, dispatch] = useReducer(CabinReducer, initialState);

  const addBooking = data => {
    dispatch({
      type: ADD_BOOKING,
      payload: data,
    });
  };

  const getSingleCabin = async id => {
    try {
      const res = await axios.get(
        `${api_string}/api/cabins/get_single_cabin/${id}`,
      );
      dispatch({
        type: GET_SINGLE_CABIN,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };

  const getCabinsByFacility = async id => {
    try {
      const res = await axios.get(
        `${api_string}/api/cabins/get_cabins_by_facility/${id}`,
      );
      dispatch({
        type: GET_CABINS_BY_FACILITY,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };

  return (
    <CabinContext.Provider
      value={{
        booking_data: state.booking_data,
        cabin: state.cabin,
        cabins: state.cabins,
        getCabinsByFacility,
        addBooking,
        getSingleCabin,
      }}
    >
      {props.children}
    </CabinContext.Provider>
  );
};

export default CabinState;
