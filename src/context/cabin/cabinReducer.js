import {
  ADD_BOOKING,
  GET_CABINS_BY_FACILITY,
  GET_SINGLE_CABIN
} from './cabin_types';

export default (state, action) => {
  switch (action.type) {
    default:
      return state;

    case ADD_BOOKING:
      localStorage.setItem('booking_data', JSON.stringify(action.payload));
      return {
        ...state,
        booking_data: action.payload
      };
    case GET_CABINS_BY_FACILITY:
      return {
        ...state,
        cabins: action.payload
      };
    case GET_SINGLE_CABIN:
      return {
        ...state,
        cabin: action.payload
      };
  }
};
