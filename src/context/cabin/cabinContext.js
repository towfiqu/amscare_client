import { createContext } from 'react';

const CabinContext = createContext();

export default CabinContext;
