import {
  TOGGLE_SIDE_MENU,
  SET_ALERT,
  REMOVE_ALERT,
  MODAL_TRIGGER,
} from './app_types';

export default (state, action) => {
  switch (action.type) {
    default:
      return state;
    case TOGGLE_SIDE_MENU:
      return {
        ...state,
        showSideMenu: !state.showSideMenu,
      };
    case SET_ALERT:
      return {
        ...state,
        alert: action.payload,
      };
    case REMOVE_ALERT:
      return {
        ...state,
        alert: null,
      };
    case MODAL_TRIGGER:
      return {
        ...state,
        modalTrigger: action.payload,
      };
  }
};
