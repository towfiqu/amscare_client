import React, { useReducer } from 'react';
import AppContext from './appContext';
import AppReducer from './appReducer';

import {
  TOGGLE_SIDE_MENU,
  SET_ALERT,
  REMOVE_ALERT,
  MODAL_TRIGGER,
} from './app_types';

const AppState = props => {
  const initialState = {
    showSideMenu: false,
    alert: null,
    modalTrigger: false,
  };

  const [state, dispatch] = useReducer(AppReducer, initialState);

  // toggle side menu
  const toggleSideMenu = () => {
    dispatch({
      type: TOGGLE_SIDE_MENU,
    });
  };

  // toggleModal
  const setModalTrigger = trigger => {
    dispatch({
      type: MODAL_TRIGGER,
      payload: trigger,
    });
  };

  // setAlert
  const setAlert = msg => {
    dispatch({
      type: SET_ALERT,
      payload: msg,
    });
  };

  const removeAlert = () => {
    setTimeout(() => {
      dispatch({ type: REMOVE_ALERT });
    }, 3000);
  };

  return (
    <AppContext.Provider
      value={{
        showSideMenu: state.showSideMenu,
        modalTrigger: state.modalTrigger,
        alert: state.alert,
        toggleSideMenu,
        setAlert,
        removeAlert,
        setModalTrigger,
      }}
    >
      {props.children}
    </AppContext.Provider>
  );
};

export default AppState;
