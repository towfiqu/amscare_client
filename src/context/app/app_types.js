export const SET_ALERT = 'SET_ALERT';
export const REMOVE_ALERT = 'REMOVE_ALERT';
export const TOGGLE_SIDE_MENU = 'TOGGLE_SIDE_MENU';
export const MODAL_TRIGGER = 'MODAL_TRIGGER';
