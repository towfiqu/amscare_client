export const REGISTER = 'REGISTER';
export const LOGIN = 'LOGIN';
export const LOAD_PATIENT = 'LOAD_PATIENT';
export const GET_PROFILE = 'GET_PROFILE';
export const CREATE_PROFILE = 'CREATE_PROFILE';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const SIGN_OUT = 'SIGN_OUT';
export const PATIENT_REPORT = 'PATIENT_REPORT';
