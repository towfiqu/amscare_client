import {
  LOGIN,
  LOAD_PATIENT,
  GET_PROFILE,
  SIGN_OUT,
  CREATE_PROFILE,
  UPDATE_PROFILE,
  PATIENT_REPORT,
} from './auth_types';

export default (state, action) => {
  switch (action.type) {
    default:
      return state;
    case LOGIN:
      localStorage.setItem('token', action.payload);
      return {
        ...state,
        isAuthenticated: true,
      };
    case LOAD_PATIENT:
      localStorage.setItem('patient_id', action.payload._id);
      return {
        ...state,
        patient: action.payload,
        isAuthenticated: true,
      };
    case CREATE_PROFILE:
      return {
        ...state,
        profile: action.payload,
      };
    case UPDATE_PROFILE:
      return {
        ...state,
        profile: action.payload,
      };
    case GET_PROFILE:
      return {
        ...state,
        profile: action.payload,
        isAuthenticated: true,
      };
    case SIGN_OUT:
      localStorage.removeItem('token');
      localStorage.removeItem('patient_id');
      return {
        ...state,
        profile: null,
        patient: null,
        isAuthenticated: false,
      };

    case PATIENT_REPORT:
      return {
        ...state,
        patient_report: action.payload,
      };
  }
};
