import React, { useReducer } from 'react';
import AuthContext from './authContext';
import AuthReducer from './authReducer';
import axios from 'axios';

import setAuthToken from '../../utils/setAuthToken';

import {
  LOGIN,
  LOAD_PATIENT,
  GET_PROFILE,
  SIGN_OUT,
  CREATE_PROFILE,
  UPDATE_PROFILE,
  PATIENT_REPORT,
} from './auth_types';

import { getApiString } from '../../config';
const api_string = getApiString();
// console.log(api_string);

const AuthState = props => {
  const initialState = {
    patient: null,
    isAuthenticated: false,
    profile: null,
    patient_report: '',
  };

  const [state, dispatch] = useReducer(AuthReducer, initialState);

  // set patient report
  const setPatientReport = report => {
    dispatch({
      type: PATIENT_REPORT,
      payload: report,
    });
  };

  //addReport
  const addReport = async (id, report) => {
    try {
      const res = await axios.post(
        `${api_string}/api/patients/add_report/${id}`,
        {
          report,
        },
      );
      if (res) {
        window.location.reload();
      }
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };

  //create profile
  const createProfile = async (id, data) => {
    try {
      const res = await axios.post(
        `${api_string}/api/patients/create_profile/${id}`,
        data,
      );
      dispatch({
        type: CREATE_PROFILE,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS', err);
    }
  };

  //update_profile
  const updateProfile = async (id, data) => {
    try {
      const res = await axios.put(
        `${api_string}/api/patients/update_profile/${id}`,
        data,
      );
      dispatch({
        type: UPDATE_PROFILE,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS', err);
    }
  };

  //login
  const login = async data => {
    localStorage.removeItem('token');
    localStorage.removeItem('patient_id');
    try {
      const res = await axios.post(`${api_string}/api/patients/login`, data);
      dispatch({
        type: LOGIN,
        payload: res.data,
      });
      if (localStorage.token) {
        setAuthToken(localStorage.token);
      }

      load_patient();
    } catch (err) {
      console.error('OOPS!!', err);
    }
  };
  //load patient
  const load_patient = async () => {
    try {
      const res = await axios.get(`${api_string}/api/patients/auth`);

      dispatch({
        type: LOAD_PATIENT,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS!!!', err);
    }
  };

  //get profile
  const getProfile = async id => {
    try {
      const res = await axios.get(
        `${api_string}/api/patients/get_profile/${id}`,
      );

      dispatch({
        type: GET_PROFILE,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOPS!!!', err);
    }
  };

  //sign out
  const signOut = () => {
    dispatch({ type: SIGN_OUT });
  };

  return (
    <AuthContext.Provider
      value={{
        patient: state.patient,
        profile: state.profile,
        isAuthenticated: state.isAuthenticated,
        patient_report: state.patient_report,
        setPatientReport,
        login,
        load_patient,
        createProfile,
        updateProfile,
        addReport,
        getProfile,
        signOut,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
