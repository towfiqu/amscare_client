import React, { useReducer } from 'react';
import FacilityContext from './facilityContext';
import FacilityReducer from './facilityReducer';

import axios from 'axios';

import {
  GET_ALL_FACILITIES,
  GET_FACILITY_BY_ID,
  GET_FACILITY_ID,
} from './facility_types';

import { getApiString } from '../../config';
const api_string = getApiString();

const FacilityState = props => {
  const initialState = {
    facility: null,
    allFacilities: [],

    facilityId: {},
  };

  const [state, dispatch] = useReducer(FacilityReducer, initialState);

  const getFacilityById = async id => {
    try {
      const res = await axios.get(
        `${api_string}/api/facilities/get_facility_by_id/${id}`,
      );
      dispatch({
        type: GET_FACILITY_BY_ID,
        payload: res.data,
      });
    } catch (err) {
      console.error('ooops!!', err);
    }
  };
  const getAllFacilities = async () => {
    try {
      const res = await axios.get(`${api_string}/api/facilities/getAll`);
      dispatch({
        type: GET_ALL_FACILITIES,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOOPS!', err);
    }
  };

  const getFacility_id = async name => {
    try {
      const res = await axios.get(
        `${api_string}/api/facilities/get_facility_id_by_name/${name}`,
      );
      dispatch({
        type: GET_FACILITY_ID,
        payload: res.data,
      });
    } catch (err) {
      console.error('ooops!!', err);
    }
  };

  return (
    <FacilityContext.Provider
      value={{
        facility: state.facility,
        allFacilities: state.allFacilities,
        facilityId: state.facilityId,
        getAllFacilities,
        getFacilityById,
        getFacility_id,
      }}
    >
      {props.children}
    </FacilityContext.Provider>
  );
};

export default FacilityState;
