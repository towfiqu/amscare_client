import {
  GET_ALL_FACILITIES,
  GET_FACILITY_BY_ID,
  GET_FACILITY_ID
} from './facility_types';

export default (state, action) => {
  switch (action.type) {
    default:
      return state;
    case GET_ALL_FACILITIES:
      return {
        ...state,
        allFacilities: action.payload
      };
    case GET_FACILITY_ID:
      return {
        ...state,
        facilityId: action.payload
      };

    case GET_FACILITY_BY_ID:
      localStorage.setItem('facility_id', action.payload._id);
      return {
        ...state,
        facility: action.payload
      };
  }
};
