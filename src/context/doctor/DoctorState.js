import React, { useReducer } from 'react';
import DoctorReducer from './doctorReducer';
import DoctorContext from './doctorContext';

import axios from 'axios';

import {
  GET_ALL_DOCTORS,
  GET_DOCTORS_BY_FACILITY,
  GET_DOCTORS_BY_FACILITY_AND_SPECIALITY,
  GET_DOCTORS_BY_FACILITY_SPECIALITY_AND_NAME,
  GET_DOCTORS_BY_NAME,
  GET_DOCTORS_BY_SPECIALITY,
  GET_DOCTOR_BY_ID,
  GET_SPECIALITIES,
  ADD_APPOINTMENT,
} from './doctor_types';

import { getApiString } from '../../config';
const api_string = getApiString();

const DoctorState = props => {
  const initialState = {
    allDoctors: [],
    doctor: [],
    allSpecialities: [],
    appointment_data: {},
  };

  const [state, dispatch] = useReducer(DoctorReducer, initialState);

  const addAppointment = data => {
    dispatch({
      type: ADD_APPOINTMENT,
      payload: data,
    });
  };

  const getAllDoctors = async () => {
    try {
      const res = await axios.get(`${api_string}/api/doctors/getAll`);
      dispatch({
        type: GET_ALL_DOCTORS,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOOPS!', err);
    }
  };
  const getDoctorById = async id => {
    try {
      const res = await axios.get(
        `${api_string}/api/doctors/get_doctor_by_id/${id}`,
      );
      dispatch({
        type: GET_DOCTOR_BY_ID,
        payload: res.data[0],
      });
    } catch (err) {
      console.error('OOOOps!!', err);
    }
  };

  const getDoctorsByFacility = async id => {
    try {
      const res = await axios.get(
        `${api_string}/api/doctors/get_doctors_by_facility/${id}`,
      );
      dispatch({
        type: GET_DOCTORS_BY_FACILITY,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };

  const getDoctorsBySpeciality = async speciality => {
    try {
      const res = await axios.get(
        `${api_string}/api/doctors/get_doctors_by_speciality/${speciality}`,
      );
      dispatch({
        type: GET_DOCTORS_BY_SPECIALITY,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };
  const getDoctorsByName = async name => {
    try {
      const res = await axios.get(
        `${api_string}/api/doctors/get_doctors_by_name/${name}`,
      );
      dispatch({
        type: GET_DOCTORS_BY_NAME,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };

  const getDoctorsByFacilityAndSpeciality = async (facility, speciality) => {
    try {
      const query = {
        facility: facility,
        speciality: speciality,
      };
      const res = await axios.post(
        `${api_string}/api/doctors/get_doctors_by_facility_and_speciality`,
        query,
      );
      dispatch({
        type: GET_DOCTORS_BY_FACILITY_AND_SPECIALITY,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPSS!!', err);
    }
  };
  const getDoctorsByFacilitySpecialityAndName = async (
    facility,
    speciality,
    name,
  ) => {
    try {
      const query = {
        facility: facility,
        speciality: speciality,
        name: name,
      };
      const res = await axios.post(
        `${api_string}/api/doctors/get_doctors_by_facility_speciality_and_name`,
        query,
      );
      dispatch({
        type: GET_DOCTORS_BY_FACILITY_SPECIALITY_AND_NAME,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOPSS!!', err);
    }
  };

  const getAllSpecialities = async () => {
    try {
      const res = await axios.get(`${api_string}/api/doctors/get_specialities`);
      dispatch({
        type: GET_SPECIALITIES,
        payload: res.data,
      });
    } catch (err) {
      console.error('OOOOPS!!', err);
    }
  };

  return (
    <DoctorContext.Provider
      value={{
        appointment_data: state.appointment_data,
        allDoctors: state.allDoctors,
        doctor: state.doctor,
        allSpecialities: state.allSpecialities,
        addAppointment,
        getAllDoctors,
        getDoctorById,
        getDoctorsByFacility,
        getDoctorsBySpeciality,
        getDoctorsByName,
        getAllSpecialities,
        getDoctorsByFacilityAndSpeciality,
        getDoctorsByFacilitySpecialityAndName,
      }}
    >
      {props.children}
    </DoctorContext.Provider>
  );
};

export default DoctorState;
