import {
  GET_ALL_DOCTORS,
  GET_DOCTORS_BY_FACILITY,
  GET_DOCTORS_BY_FACILITY_AND_SPECIALITY,
  GET_DOCTORS_BY_FACILITY_SPECIALITY_AND_NAME,
  GET_DOCTORS_BY_NAME,
  GET_DOCTORS_BY_SPECIALITY,
  GET_DOCTOR_BY_ID,
  GET_SPECIALITIES,
  ADD_APPOINTMENT
} from './doctor_types';

export default (state, action) => {
  switch (action.type) {
    default:
      return state;
    case GET_ALL_DOCTORS:
      return {
        ...state,
        allDoctors: action.payload
      };
    case GET_DOCTORS_BY_FACILITY:
      return {
        ...state,
        allDoctors: action.payload
      };
    case GET_DOCTORS_BY_SPECIALITY:
      return {
        ...state,
        allDoctors: action.payload
      };
    case GET_SPECIALITIES:
      return {
        ...state,
        allSpecialities: action.payload
      };
    case GET_DOCTORS_BY_FACILITY_AND_SPECIALITY:
      return {
        ...state,
        allDoctors: action.payload
      };
    case GET_DOCTORS_BY_FACILITY_SPECIALITY_AND_NAME:
      return {
        ...state,
        allDoctors: action.payload
      };
    case GET_DOCTORS_BY_NAME:
      return {
        ...state,
        allDoctors: action.payload
      };
    case GET_DOCTOR_BY_ID:
      localStorage.setItem('doctor_id', action.payload._id);
      return {
        ...state,
        doctor: action.payload
      };
    case ADD_APPOINTMENT:
      localStorage.setItem('aptData', JSON.stringify(action.payload));
      return {
        ...state,
        appointment_data: action.payload
      };
  }
};
