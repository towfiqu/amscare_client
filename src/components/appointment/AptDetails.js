import React, { useContext, useEffect, useState } from 'react';
import axios from 'axios';
import ConfirmAptModal from './ConfirmAptModal';
import DoctorContext from '../../context/doctor/doctorContext';

import { Link } from 'react-router-dom';

import { getApiString } from '../../config';
const api_string = getApiString();

const AptDetails = () => {
  const doctorContext = useContext(DoctorContext);
  const { addAppointment, appointment_data } = doctorContext;

  const [modalTrigger, setModalTrigger] = useState(false);

  const onConfirm = async () => {
    const data = {
      name: appointment_data.name,
      email: appointment_data.email,
      phone: appointment_data.phone,
      date: appointment_data.date,
      doctor: appointment_data.doctor,
      speciality: appointment_data.speciality,
      facility: appointment_data.facility,
      location: appointment_data.location,
    };
    try {
      const res = await axios.post(
        `${api_string}/api/appointments/add_appointment`,
        data,
      );
      if (res.data) {
        // send mail before triggering the modal
        setModalTrigger(!modalTrigger);
      }
    } catch (err) {
      console.error('ooops!!', err);
    }
  };
  useEffect(() => {
    const aptData = JSON.parse(localStorage.getItem('aptData'));
    addAppointment(aptData);

    // eslint-disable-next-line
  }, []);
  if (modalTrigger) return <ConfirmAptModal email={appointment_data.email} />;
  return (
    <div className='backdrop'>
      <div className='container'>
        <h3 className='preview-heading'>Appointment Details</h3>

        <div className='preview-wrapper'>
          <div className='top-heading'>
            <span
              style={{
                fontSize: '24px',
                color: '#004579',
              }}
            >
              {appointment_data.date}
            </span>
          </div>
          <div className='main-content'>
            <div className='content-left'>
              <h4 className='content-heading'>Patient info</h4>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>{appointment_data.name}</span>
              </p>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>{appointment_data.email}</span>
              </p>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>{appointment_data.phone}</span>
              </p>
            </div>
            <div className='content-right'>
              <h4 className='content-heading'>Doctor Info</h4>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>{appointment_data.doctor}</span>
              </p>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>{appointment_data.location}</span>
              </p>
            </div>
          </div>
          <div className='action-wrapper'>
            <Link className='btn-cancel' to='/apt'>
              Cancel
            </Link>

            <button className='btn-confirm' onClick={onConfirm}>
              Confirm
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AptDetails;
