import React from 'react';
import history from '../../history';

const ConfirmAptModal = ({ email }) => {
  return (
    <div className='backdrop'>
      <div className='negative-alert'>
        <p className='mv-10'>
          Your appointment has been booked. A confirmation email has been sent
          to <span style={{ fontSize: '18px', color: '#004579' }}>{email}</span>
        </p>
        <button onClick={() => history.push('/')}>Return to Home page</button>
      </div>
    </div>
  );
};

export default ConfirmAptModal;
