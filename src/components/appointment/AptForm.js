import React, { useEffect, useContext, useState } from 'react';
import history from '../../history';
import DoctorContext from '../../context/doctor/doctorContext';
import FacilityContext from '../../context/facility/facilityContext';
import AppContext from '../../context/app/appContext';
import Select from 'react-select';

const AptForm = () => {
  const doctorContext = useContext(DoctorContext);
  const facilityContext = useContext(FacilityContext);
  const appContext = useContext(AppContext);
  const { setAlert, alert, removeAlert } = appContext;
  const { getDoctorById, doctor, addAppointment } = doctorContext;

  const { getFacilityById, facility } = facilityContext;

  useEffect(() => {
    localStorage.removeItem('aptData');
    const doctor_id = localStorage.getItem('doctor_id');
    const facility_id = localStorage.getItem('facility_id');
    getDoctorById(doctor_id);
    getFacilityById(facility_id);

    // eslint-disable-next-line
  }, []);

  const [apt, setApt] = useState({
    patient_name: '',
    patient_email: '',
    patient_phone: '',
  });

  const { patient_name, patient_email, patient_phone } = apt;

  const onChange = e => {
    setApt({ ...apt, [e.target.name]: e.target.value });
  };

  const [hours, setHours] = useState('');

  const onSetHours = hours => {
    setHours(hours);
  };

  // setting appointment time
  // taking doctor business hours and trimming it to just time
  // e.g - if doctor business hours is set to - Monday: 7pm to 10pm
  // this function will convert this string to - Monday: 7pm
  const aptTime = str => {
    const getIndex = str.indexOf('to');
    const newStr = str.substring(0, getIndex);
    return `This ${newStr}`;
  };

  let options;
  if (doctor.business_hours) {
    options = doctor.business_hours.map(item => {
      const newItem = aptTime(item);
      return {
        value: newItem,
        label: newItem,
      };
    });
  }

  const onNext = () => {
    const aptData = {
      name: patient_name,
      email: patient_email,
      phone: patient_phone,
      date: hours.value,
      doctor: doctor.doctor_name,
      speciality: doctor.doctor_speciality,
      facility: facility.facility_name,
      location: facility.location.facility_address,
    };
    addAppointment(aptData);
    setTimeout(() => {
      if (
        patient_name !== '' &&
        patient_phone !== '' &&
        patient_email !== '' &&
        hours !== ''
      ) {
        history.push('/confirm_apt');
      } else {
        setAlert('Please fill the form correctly!');

        removeAlert();
      }
    }, 300);
  };

  const styles = {
    container: (provided, state) => ({
      ...provided,
      marginTop: '10px',
    }),
    input: (provided, state) => ({
      ...provided,
      padding: '10px 0',
    }),
  };

  return (
    <div className='container wrapper'>
      {alert && <h2 className='alert'>{alert}</h2>}
      <div className='input-form-wrapper'>
        <h3
          style={{
            color: 'grey',
            textTransform: 'uppercase',
            letterSpacing: '1.1px',
          }}
        >
          Schedule an appointment
        </h3>
        <p className='mv-10'>
          Please enter your details before confirming the appointment
        </p>
        <div>
          <input
            type='text'
            className='input-form'
            placeholder='Name'
            name='patient_name'
            value={patient_name}
            onChange={onChange}
          />
        </div>
        <div>
          <input
            type='email'
            className='input-form'
            placeholder='Email'
            name='patient_email'
            value={patient_email}
            onChange={onChange}
          />
        </div>
        <div>
          <input
            type='text'
            className='input-form'
            placeholder='Contact number'
            name='patient_phone'
            value={patient_phone}
            onChange={onChange}
          />
        </div>
        <Select
          placeholder='Appointment Date'
          onChange={onSetHours}
          value={hours}
          options={options}
          styles={styles}
        />
        <button onClick={onNext} className='input-form submit'>
          Next
        </button>
      </div>
    </div>
  );
};

export default AptForm;
