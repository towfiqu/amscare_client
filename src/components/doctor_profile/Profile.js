import React, { useEffect, useContext } from 'react';
import history from '../../history';
import DoctorContext from '../../context/doctor/doctorContext';
import FacilityContext from '../../context/facility/facilityContext';

const Profile = () => {
  const doctorContext = useContext(DoctorContext);
  const facilityContext = useContext(FacilityContext);
  const { getDoctorById, doctor } = doctorContext;
  const { getFacilityById, facility } = facilityContext;

  useEffect(() => {
    window.scrollTo(0, 0); /* to stop the page from scrolling */
    const doctor_id = localStorage.getItem('doctor_id');
    getDoctorById(doctor_id);
    const facility_id = localStorage.getItem('facility_id');
    if (!facility_id) {
      getFacilityById(doctor.facility_id);
    } else {
      getFacilityById(facility_id);
    }
    // eslint-disable-next-line
  }, []);
  return (
    <div className='container wrapper'>
      <div className='doctor-profile-top'>
        <img
          style={{
            borderRadius: '50%',
            border: '1px solid grey',
          }}
          src={doctor.doctor_image}
          alt=''
        />
        <div className='doctor-profile-top-right'>
          <div className='doctor-title'>
            <h2>{doctor.doctor_name}</h2>
            {[doctor.doctor_speciality].map((speciality, index) => (
              <p key={index} style={{ marginLeft: '10px' }}>
                {(index ? ',' : '') + speciality}
              </p>
            ))}
          </div>

          <button onClick={() => history.push('/apt')} className='book-apt-btn'>
            Book Appointment
          </button>
        </div>
      </div>
      <div>
        <p
          style={{
            margin: '20px 0px 40px 0',
            lineHeight: '25px',
            fontSize: '16px',
            fontWeight: '400',
            color: '#333',
          }}
        >
          {doctor.doctor_bio}
        </p>
      </div>
      <div className='doctor-profile-bottom'>
        <div className='doctor-profile-bottom-left'>
          <h1
            style={{
              marginBottom: '20px',
              color: 'grey',
              fontSize: '16px',
              fontWeight: '500',
              textTransform: 'uppercase',
              letterSpacing: '1.2px',
            }}
          >
            <span
              style={{
                color: '#004579',
              }}
            >
              Practice Facility{' '}
            </span>{' '}
            | {facility && facility.facility_name}
          </h1>
          <img
            className='facility-img'
            onClick={() => history.push('/facility')}
            src={facility && facility.facility_image}
            alt=''
          />
        </div>
        <div className='doctor-profile-bottom-right'>
          <h2
            style={{
              textTransform: 'uppercase',
              letterSpacing: '1.1px',
              marginTop: '30px',
              marginBottom: '10px',
              color: 'grey',
            }}
          >
            Contact
          </h2>
          <div className='doctor-contact-content-wrapper'>
            <div className='doctor-contact-content'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                height='24'
                viewBox='0 0 24 24'
                width='24'
              >
                <path d='M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z' />
                <path d='M0 0h24v24H0z' fill='none' />
              </svg>
              <p style={{ marginLeft: '10px' }}>{doctor.doctor_email}</p>
            </div>
            <div className='doctor-contact-content'>
              <svg
                xmlns='http://www.w3.org/2000/svg'
                height='24'
                viewBox='0 0 24 24'
                width='24'
              >
                <path d='M0 0h24v24H0z' fill='none' />
                <path d='M6.62 10.79c1.44 2.83 3.76 5.14 6.59 6.59l2.2-2.2c.27-.27.67-.36 1.02-.24 1.12.37 2.33.57 3.57.57.55 0 1 .45 1 1V20c0 .55-.45 1-1 1-9.39 0-17-7.61-17-17 0-.55.45-1 1-1h3.5c.55 0 1 .45 1 1 0 1.25.2 2.45.57 3.57.11.35.03.74-.25 1.02l-2.2 2.2z' />
              </svg>
              <p style={{ marginLeft: '10px' }}>{doctor.doctor_phone}</p>
            </div>
            <div className='doctor-time-content'>
              <p
                style={{
                  color: 'grey',
                  textTransform: 'uppercase',
                  letterSpacing: '1px',
                  margin: '20px',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}
              >
                Visiting Hours
              </p>
              {doctor.business_hours &&
                doctor.business_hours.map((hours, index) => (
                  <div key={index} className='doctor-contact-content'>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      height='24'
                      viewBox='0 0 24 24'
                      width='24'
                    >
                      <path d='M0 0h24v24H0V0z' fill='none' />
                      <path d='M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm.5-13H11v6l5.25 3.15.75-1.23-4.5-2.67z' />
                    </svg>
                    <p style={{ marginLeft: '10px' }}>
                      {(index ? '\n' : '') + hours}
                    </p>
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Profile;
