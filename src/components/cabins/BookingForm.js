import React, { useState, useEffect, useContext } from 'react';
import history from '../../history';
import CabinContext from '../../context/cabin/cabinContext';
import FacilityContext from '../../context/facility/facilityContext';
import AppContext from '../../context/app/appContext';

const BookingForm = () => {
  const cabinContext = useContext(CabinContext);
  const facilityContext = useContext(FacilityContext);
  const appContext = useContext(AppContext);
  const { setAlert, removeAlert, alert } = appContext;
  const { getSingleCabin, cabin, addBooking } = cabinContext;
  const { getFacilityById, facility } = facilityContext;
  useEffect(() => {
    const cabin_id = localStorage.getItem('cabin_id');
    const facility_id = localStorage.getItem('facility_id');

    getSingleCabin(cabin_id);

    getFacilityById(facility_id);

    // eslint-disable-next-line
  }, []);
  const [book, setBook] = useState({
    patient_name: '',
    patient_email: '',
    patient_phone: '',
  });
  const { patient_name, patient_email, patient_phone } = book;
  const onChange = e => {
    setBook({ ...book, [e.target.name]: e.target.value });
  };
  const onNext = () => {
    const data = {
      patient_name: patient_name,
      patient_email: patient_email,
      patient_phone: patient_phone,
      facility: facility.facility_name,
      cabin_number: cabin.cabin_number,
      cabin_cost: cabin.cabin_cost,
      cabin_id: cabin._id,
      facility_id: facility._id,
    };
    addBooking(data);
    setTimeout(() => {
      if (patient_name !== '' && patient_email !== '' && patient_phone !== '') {
        history.push('/cabin_details');
      } else {
        setAlert('Please Fill the form properly');
        setTimeout(() => {
          removeAlert();
        }, 3000);
      }
    }, 300);
  };
  return (
    <div className='container wrapper'>
      {alert && <h2 className='alert'>{alert}</h2>}
      <div className='input-form-wrapper'>
        <h3
          style={{
            color: 'grey',
            textTransform: 'uppercase',
            letterSpacing: '1.1px',
          }}
        >
          Book your cabin
        </h3>
        <p className='mv-10'>
          Please enter your details before confirming the Book
        </p>
        <div>
          <input
            type='text'
            className='input-form'
            placeholder='Name'
            name='patient_name'
            value={patient_name}
            onChange={onChange}
          />
        </div>
        <div>
          <input
            type='email'
            className='input-form'
            placeholder='Email'
            name='patient_email'
            value={patient_email}
            onChange={onChange}
          />
        </div>
        <div>
          <input
            type='text'
            className='input-form'
            placeholder='Contact number'
            name='patient_phone'
            value={patient_phone}
            onChange={onChange}
          />
        </div>
        <button onClick={onNext} className='input-form submit'>
          Next
        </button>
      </div>
    </div>
  );
};

export default BookingForm;
