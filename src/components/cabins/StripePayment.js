import React from 'react';
import history from '../../history';
import StripeCheckout from 'react-stripe-checkout';
import axios from 'axios';
import { stripe_key } from '../../config';

import { getApiString } from '../../config';
const api_string = getApiString();

const StripePayment = ({
  cabin_cost,
  patient_email,
  facility_image,
  facility,
  patient_name,
  patient_phone,
  cabin_number,
  cabin_id,
  facility_id,
}) => {
  const onToken = async token => {
    const data = {
      token: token,
      amount: cabin_cost,
    };
    try {
      const res = await axios.post(`${api_string}/api/cabins/payment`, data);
      if (res.data) {
        const data = {
          patient_name: patient_name,
          patient_email: patient_email,
          patient_phone: patient_phone,
          facility: facility,
          cabin_number: cabin_number,
          cabin_cost: cabin_cost,
          cabin_id: cabin_id,
          facility_id: facility_id,
        };
        const res = await axios.post(
          `${api_string}/api/cabins/book_cabin`,
          data,
        );
        if (res.data) {
          history.push('/confirmPayment');
        } else {
          alert('Something went wrong with your booking!!');
        }
      }
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };

  return (
    <div>
      <StripeCheckout
        name={facility}
        image={facility_image}
        email={patient_email}
        amount={cabin_cost}
        currency='USD'
        token={onToken}
        stripeKey={stripe_key}
        description={`Cabin Cost ${cabin_cost}Tk`}
        label='Confirm & Pay'
      />
    </div>
  );
};

export default StripePayment;
