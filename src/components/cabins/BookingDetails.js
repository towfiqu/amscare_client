import React, { useContext, useEffect } from 'react';
import CabinContext from '../../context/cabin/cabinContext';
import FacilityContext from '../../context/facility/facilityContext';
import StripePayment from './StripePayment';

import { Link } from 'react-router-dom';

const BookingDetails = () => {
  const cabinContext = useContext(CabinContext);
  const facilityContext = useContext(FacilityContext);
  const { addBooking, booking_data } = cabinContext;
  const { getFacilityById, facility } = facilityContext;
  useEffect(() => {
    const booking_data = JSON.parse(localStorage.getItem('booking_data'));
    addBooking(booking_data);
    const facility_id = localStorage.getItem('facility_id');
    getFacilityById(facility_id);
    // eslint-disable-next-line
  }, []);

  return (
    <div className='backdrop'>
      <div className='container'>
        <h2 className='preview-heading'>Booking Details</h2>
        <p style={{ textAlign: 'center', marginTop: '5px', color: '#ccc' }}>
          Please note that Cost will be converted to US dollar value
        </p>
        <div className='preview-wrapper'>
          <div className='top-heading'>
            <span
              style={{
                fontSize: '24px',
                color: '#004579',
              }}
            >
              {booking_data.cabin_cost}Tk{' '}
            </span>
            <span> / day</span>
          </div>
          <div className='main-content'>
            <div className='content-left'>
              <h4 className='content-heading'>Patient info</h4>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>{booking_data.patient_name}</span>
              </p>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>{booking_data.patient_email}</span>
              </p>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>{booking_data.patient_phone}</span>
              </p>
            </div>
            <div className='content-right'>
              <h4 className='content-heading'>Cabin Info</h4>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>{booking_data.facility}</span>
              </p>
              <p className='content-info-p'>
                {' '}
                <span>
                  <svg
                    viewBox='0 0 32 32'
                    xmlns='http://www.w3.org/2000/svg'
                    width='20'
                    height='20'
                  >
                    <path d='M16 2a14 14 0 1014 14A14 14 0 0016 2zm0 26a12 12 0 1112-12 12 12 0 01-12 12z' />
                    <path d='M13.38 19.59l-3.29-3.3-1.42 1.42L12 21a2 2 0 001.42.58 2 2 0 001.37-.58l8.54-8.54-1.42-1.41z' />
                  </svg>
                </span>{' '}
                <span className='ml-5'>
                  Cabin NO.{' '}
                  <span style={{ fontWeight: '700' }}>
                    {booking_data.cabin_number}
                  </span>
                </span>
              </p>
            </div>
          </div>
          <div className='action-wrapper'>
            <Link className='btn-cancel' to='/facility'>
              Cancel
            </Link>

            <StripePayment /* all the necessary props for stripe payment */
              cabin_cost={booking_data.cabin_cost}
              patient_email={booking_data.patient_email}
              facility_image={facility && facility.facility_image}
              facility={booking_data.facility}
              patient_name={booking_data.patient_name}
              patient_phone={booking_data.patient_phone}
              cabin_number={booking_data.cabin_number}
              cabin_id={booking_data.cabin_id}
              facility_id={booking_data.facility_id}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default BookingDetails;
