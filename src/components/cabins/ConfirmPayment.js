import React from 'react';
import history from '../../history';

const ConfirmPayment = () => {
  return (
    <div className='backdrop'>
      <div className='negative-alert'>
        <h2 style={{ color: '#004579' }}>Cabin Booked</h2>
        <p className='mv-10'>
          Your Cabin has been booked. Thanks for doing business with us.
        </p>
        <button onClick={() => history.push('/')}>Return to Home page</button>
      </div>
    </div>
  );
};

export default ConfirmPayment;
