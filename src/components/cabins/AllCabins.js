import React, { useContext, useEffect } from 'react';
import history from '../../history';
import CabinContext from '../../context/cabin/cabinContext';
import FacilityContext from '../../context/facility/facilityContext';
const AllCabins = () => {
  const cabinContext = useContext(CabinContext);
  const facilityContext = useContext(FacilityContext);

  const { cabins, getCabinsByFacility } = cabinContext;
  const { getFacilityById, facility } = facilityContext;

  useEffect(() => {
    const facility_id = localStorage.getItem('facility_id');
    getCabinsByFacility(facility_id);
    getFacilityById(facility_id);
    //eslint-disable-next-line
  }, []);

  const onNext = cabin_id => {
    localStorage.setItem('cabin_id', cabin_id);
    history.push('/cabinForm');
  };

  return (
    <div className='container wrapper'>
      <h1
        style={{
          margin: '20px 0',
          color: 'grey',
          fontSize: '16px',
          fontWeight: '500',
          textTransform: 'uppercase',
          letterSpacing: '1.2px',
        }}
      >
        <span
          style={{
            color: '#004579',
          }}
        >
          Cabins{' '}
        </span>{' '}
        | {facility && facility.facility_name}
      </h1>
      <div className='cabins-container'>
        {cabins.map(cabin => (
          <div className='single-cabin-container' key={cabin._id}>
            <img
              className='cabin-img'
              src={
                cabin.cabin_image
                  ? cabin.cabin_image
                  : 'https://pbs.twimg.com/media/Dj1BPeeU8AA-Ar8.jpg'
              }
              alt=''
            />
            <div className='single-cabin-info'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                }}
              >
                {cabin.cabin_booked ? (
                  <span className='not-available-badge'>not available </span>
                ) : (
                  <span className='available-badge'>available </span>
                )}

                <span
                  style={{
                    color: 'grey',
                    fontSize: '14px',
                    fontWeight: 'normal',
                    marginLeft: '10px',
                  }}
                >
                  {cabin.cabin_beds === 1
                    ? `${cabin.cabin_beds} BED`
                    : `${cabin.cabin_beds} BEDS`}{' '}
                  &bull; {cabin.cabin_AC === 'yes' ? 'AC' : 'NON-AC'}
                </span>
              </div>
              <div className='cabin-cost'>
                {cabin.cabin_cost}{' '}
                <span
                  style={{
                    color: 'grey',
                    fontSize: '14px',
                    fontWeight: 'normal',
                  }}
                >
                  / day
                </span>
              </div>
              {cabin.cabin_booked ? (
                <button className='book-now-btn-disabled'>BOOK NOW</button>
              ) : (
                <button
                  onClick={() => onNext(cabin._id)}
                  className='book-now-btn'
                >
                  BOOK NOW
                </button>
              )}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default AllCabins;
