import React, { useContext } from 'react';
import history from '../../history';
import FacilityContext from '../../context/facility/facilityContext';

const FoundFacilities = ({ all_facilities }) => {
  const facilityContext = useContext(FacilityContext);
  const { getFacilityById } = facilityContext;

  const getFacility = id => {
    localStorage.removeItem('facility_id');
    setTimeout(() => {
      history.push('/facility');
    }, 100);

    getFacilityById(id);
  };

  return (
    <div className='all-facilities-container-nearest'>
      {all_facilities &&
        all_facilities.map(facility => (
          <div
            onClick={() => getFacility(facility._id)}
            key={facility._id}
            className='single-facility'
          >
            <img src={facility.facility_image} alt='' />
            <div className='single-facility-container'>
              <h3>{facility.facility_name}</h3>
              <p>{facility.location.facility_address}</p>
            </div>
          </div>
        ))}
    </div>
  );
};

export default FoundFacilities;
