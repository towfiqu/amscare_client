import React, { useState } from 'react';
import { LoadScript, Autocomplete } from '@react-google-maps/api';
import { map_api } from '../../config';

import axios from 'axios';

import FoundFacilities from './FoundFacilities';

import { getApiString } from '../../config';
const api_string = getApiString();

const places = ['places'];
let all_facilities = [];
const LocationInput = () => {
  const [address, setAddress] = useState('');
  const [name, setName] = useState('');
  const [lat, setLat] = useState('');
  const [lng, setLng] = useState('');

  const [facilityFound, setFacilityFound] = useState('1');

  const onLoad = address => {
    setAddress(address);
  };

  const onPlaceChange = () => {
    if (address !== '') {
      setName(address.getPlace().formatted_address);
      setLat(address.getPlace().geometry.location.lat());
      setLng(address.getPlace().geometry.location.lng());
    } else {
      console.log('Address is not loaded yet');
    }
  };

  const onFind = async () => {
    const data = {
      location: {
        coordinates: [parseFloat(lng), parseFloat(lat)],
        address: name,
      },
    };
    const res = await axios.get(
      `${api_string}/api/facilities/find_nearby_facilities?lat=${data.location.coordinates[1]}&lng=${data.location.coordinates[0]}`,
    );
    all_facilities = res.data;
    if (all_facilities) {
      setFacilityFound('2');
    }
    if (all_facilities.length < 1) {
      setFacilityFound('3');
    }
  };

  return (
    <div
      style={{
        background: 'rgba(0, 0, 0, 0.8)',
      }}
    >
      <div className='container wrapper'>
        <div className='temp-container'>
          <LoadScript googleMapsApiKey={map_api} libraries={places}>
            <a href='/' className='return-link'>
              Return to Home
            </a>
            <h2 className='nearest-page-heading'>
              Find facilities 5km radius to your location
            </h2>
            <Autocomplete onPlaceChanged={onPlaceChange} onLoad={onLoad}>
              <input
                className='input-form'
                type='text'
                name='location_input'
                placeholder='Enter your location'
              />
            </Autocomplete>
            <button onClick={onFind} className='input-form submit mb-20'>
              Find Nearest Facilities
            </button>
          </LoadScript>
          {facilityFound === '2' || facilityFound === '1' ? (
            <FoundFacilities all_facilities={all_facilities} />
          ) : (
            <>
              <div className='negative-alert'>
                <p>No Medical Facilities found near this location.</p>
                <button onClick={() => window.location.reload()}>OK</button>
              </div>
              <div className='blur-bg'></div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default LocationInput;
