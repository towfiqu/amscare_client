import React, { useContext, useState, useEffect } from 'react';
import FacilityContext from '../../context/facility/facilityContext';
import Select from 'react-select';
import ShowFacilities from './ShowFacilities';

const SearchFacilities = () => {
  const facilityContext = useContext(FacilityContext);
  const {
    getAllFacilities,
    allFacilities,
    getFacilityById,
    facility,
  } = facilityContext;

  useEffect(() => {
    getAllFacilities();

    // eslint-disable-next-line
  }, []);
  const [selectFacility, setSelectFacility] = useState('');
  const onSelect = selectFacility => {
    setSelectFacility(selectFacility);
    getFacilityById(selectFacility.value);
  };
  const options = allFacilities.map(facility => ({
    value: facility._id,
    label: facility.facility_name,
  }));
  const styles = {
    input: (provided, state) => ({
      ...provided,
      height: '40px',
    }),
  };
  return (
    <div className='container wrapper'>
      <div className='facilities-header'>
        <h2
          style={{
            fontSize: '16px',
            fontWeight: '500',
            color: 'grey',
            textTransform: 'uppercase',

            letterSpacing: '1.2px',
          }}
        >
          Find Medical Facilities
        </h2>
        <div className='select-dropdown'>
          <Select
            onChange={onSelect}
            value={selectFacility}
            options={options}
            placeholder='Select A Facility ...'
            styles={styles}
          />
        </div>
      </div>
      <ShowFacilities facility={facility} />
      {/* Will render facilities based on search filter */}
    </div>
  );
};

export default SearchFacilities;
