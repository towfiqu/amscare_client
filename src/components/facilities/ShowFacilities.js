import React, { useContext, useEffect } from 'react';
import history from '../../history';
import FacilityContext from '../../context/facility/facilityContext';

const ShowFacilities = ({ facility }) => {
  // Props coming from SearchFacilities
  const facilityContext = useContext(FacilityContext);
  const { getAllFacilities, allFacilities, getFacilityById } = facilityContext;
  useEffect(() => {
    getAllFacilities();
    // eslint-disable-next-line
  }, []);
  const getFacility = id => {
    localStorage.removeItem('facility_id');
    setTimeout(() => {
      history.push('/facility');
    }, 100);

    getFacilityById(id);
  };
  if (facility)
    return (
      <div className='all-facilities-container'>
        <div
          onClick={() => getFacility(facility._id)}
          className='single-facility'
        >
          <img src={facility.facility_image} alt='' />
          <div className='single-facility-container'>
            <h3>{facility.facility_name}</h3>
            <p>{facility.location.facility_address}</p>
          </div>
        </div>
      </div>
    );
  else {
    return (
      <div className='all-facilities-container'>
        {allFacilities.map(facility => (
          <div
            onClick={() => getFacility(facility._id)}
            key={facility._id}
            className='single-facility'
          >
            <img src={facility.facility_image} alt='' />
            <div className='single-facility-container'>
              <h3>{facility.facility_name}</h3>
              <p>{facility.location.facility_address}</p>
            </div>
          </div>
        ))}
      </div>
    );
  }
};

export default ShowFacilities;
