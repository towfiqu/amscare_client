import React, { useContext } from 'react';
import DoctorContext from '../../context/doctor/doctorContext';
import FacilityContext from '../../context/facility/facilityContext';
import history from '../../history';

const ShowDoctors = ({ fromNav }) => {
  const doctorContext = useContext(DoctorContext);
  const facilityContext = useContext(FacilityContext);
  const { getDoctorById, allDoctors } = doctorContext;
  const { getFacilityById } = facilityContext;
  const getDoctor = (doc_id, faci_id) => {
    localStorage.removeItem('doctor_id');
    localStorage.removeItem('facility_id');
    setTimeout(() => {
      history.push('/doctor');
    }, 500);
    getDoctorById(doc_id);
    getFacilityById(faci_id);
  };

  return (
    <div className='container'>
      <section className='show-doctors-section'>
        <h1>{fromNav ? 'All Doctors' : 'Results'}</h1>
        <p className='mv-10' style={{ fontSize: '14px' }}>
          Choose a doctor from below and book an appointment.
        </p>
      </section>
      <section className='show-doctors-section-content'>
        {/* making this clickable and go to doc profile*/}
        {allDoctors.map(doctor => (
          <div
            className='show-doctors-section-individual-content doc-card'
            key={doctor._id}
            onClick={() => getDoctor(doctor._id, doctor.facility_id)}
          >
            <img src={doctor.doctor_image} alt='' />
            <div className='pv-10 ph-15'>
              <h4>{doctor.doctor_name}</h4>
              {doctor.doctor_speciality.map((speciality, index) => (
                <p style={{ display: 'inline-block' }} key={index}>
                  {(index ? ', ' : '') + speciality}
                </p>
              ))}
            </div>
          </div>
        ))}
      </section>
    </div>
  );
};

export default ShowDoctors;
