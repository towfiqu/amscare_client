import React, { useContext } from 'react';
import history from '../../history';
import AppContext from '../../context/app/appContext';
import AuthContext from '../../context/auth/authContext';

import { Link } from 'react-router-dom';

const Navbar = () => {
  const appContext = useContext(AppContext);
  const authContext = useContext(AuthContext);
  const { toggleSideMenu } = appContext;
  const { isAuthenticated, signOut } = authContext;

  const onSignOut = () => {
    signOut();
    history.push('/');
    window.location.reload();
  };

  return (
    <div>
      <header>
        <ul className='container-2'>
          <li onClick={() => history.push('/')} className='logo nav-items'>
            AMS
          </li>
          <li className='nav-items nav-items-lg'>
            <Link to='/find'>Find</Link>
          </li>

          <li className='nav-items nav-items-lg'>
            <Link to='/facilities'>Facilities</Link>
          </li>
          <li className='nav-items nav-items-lg'>
            <Link to='/medical_tests'>Lab Tests</Link>
          </li>
          <li className='nav-items nav-items-lg'>
            <Link to='/nearest_facilities'>Nearest Facilities</Link>
          </li>

          <li className='nav-items nav-items-lg nav-items-btn'>
            <button
              onClick={() => history.push('/login')}
              className='btn btn-nav'
            >
              My Profile
            </button>
          </li>

          {isAuthenticated && (
            <li className='nav-items nav-items-lg nav-items-btn'>
              <button onClick={onSignOut} className='btn btn-sign-out'>
                Sign Out
              </button>{' '}
            </li>
          )}

          <li className='side-nav-icon'>
            <span onClick={() => toggleSideMenu()}>
              <i className='material-icons'>menu</i>
            </span>
          </li>
        </ul>
      </header>
    </div>
  );
};

export default Navbar;
