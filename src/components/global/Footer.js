import React from 'react';
const Footer = () => {
  return (
    <div>
      <footer>
        <p>Copyright &copy; 2020 by ams | All rights reserved</p>
      </footer>
    </div>
  );
};

export default Footer;
