import React, { useContext } from 'react';
import AppContext from '../../context/app/appContext';
import AuthContext from '../../context/auth/authContext';

import history from '../../history';

import { Link } from 'react-router-dom';

const SideNav = () => {
  const appContext = useContext(AppContext);
  const authContext = useContext(AuthContext);

  const { isAuthenticated, signOut } = authContext;

  const { toggleSideMenu, showSideMenu } = appContext;
  let sideNavClass = 'side-nav';
  if (showSideMenu) {
    sideNavClass = 'side-nav open';
  }

  const onSignOut = () => {
    signOut();
    history.push('/');
  };

  return (
    <div className={sideNavClass}>
      <ul>
        <i
          onClick={() => toggleSideMenu()}
          className='material-icons side-nav-close'
        >
          close
        </i>

        <li>
          <Link onClick={() => toggleSideMenu()} to='/find'>
            Find
          </Link>
        </li>

        <li>
          <Link onClick={() => toggleSideMenu()} to='/facilities'>
            Facilities
          </Link>
        </li>
        <li>
          <Link onClick={() => toggleSideMenu()} to='/medical_tests'>
            Medical Tests
          </Link>
        </li>
        <li>
          <Link onClick={() => toggleSideMenu()} to='/nearest_facilities'>
            Nearest Facilities
          </Link>
        </li>
        <div
          className='my-profile mt-30 mb-0 mh-20'
          onClick={() => history.push('/login')}
        >
          <span onClick={() => toggleSideMenu()}>My Profile</span>
        </div>
        {isAuthenticated && (
          <div className='sign-out mt-20 mb-0 mh-20' onClick={onSignOut}>
            <span onClick={() => toggleSideMenu()}>Sign Out</span>
          </div>
        )}
      </ul>
    </div>
  );
};

export default SideNav;
