import React, { useContext, useEffect } from 'react';
import TestContext from '../../context/test/testContext';
const Pagination = () => {
  const testContext = useContext(TestContext);
  const { getPageNumbers, page_numbers, tests, setPage, page } = testContext;

  useEffect(() => {
    getPageNumbers();
    // eslint-disable-next-line
  }, [tests.length > 0]);
  return (
    <div className='container'>
      <div className='pagination'>
        <span
          onClick={() => setPage(page > 1 ? page - 1 : 1)}
          className='page-number'
        >
          Prev
        </span>
        {page_numbers.length > 0 &&
          page_numbers.map(number => (
            <span
              onClick={() => setPage(number)}
              key={number}
              className='page-number'
            >
              {number}
            </span>
          ))}
        <span
          onClick={() =>
            setPage(page < page_numbers.length ? page + 1 : page_numbers.length)
          }
          className='page-number'
        >
          Next
        </span>
      </div>
    </div>
  );
};

export default Pagination;
