import React, { useContext } from 'react';
import Pagination from './Pagination';
import TestContext from '../../context/test/testContext';

const ShowTests = ({ tests }) => {
  const testContext = useContext(TestContext);
  const { page } = testContext;
  // pagination logic
  const per_page = 3;
  const start = page * per_page - per_page;
  const end = start + 3;

  return (
    <div>
      {/* <Pagination /> */}
      {/* <p style={{ textAlign: 'center' }}>
        Total {tests.length} Tests. (Page {page})
      </p> */}
      <div className='tests-wrapper'>
        {tests &&
          tests.slice(start, end).map(test => (
            <div key={test._id} className='test'>
              <p className='cost'>{test.test_cost}Tk</p>
              <h3 className='test-name'>{test.test_name}</h3>
              <br />
              <h4 style={{ color: '#004579' }}>{test.facility}</h4>
              <hr />
              <p>{test.location}</p>
            </div>
          ))}
      </div>
      <Pagination />
      <p style={{ textAlign: 'center' }}>
        Total {tests.length} Tests. (Page {page})
      </p>
    </div>
  );
};

export default ShowTests;
