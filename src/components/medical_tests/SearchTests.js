import React, { useState, useContext, useEffect } from 'react';
import TestContext from '../../context/test/testContext';
import ShowTests from './ShowTests';

const SearchTests = () => {
  const testContext = useContext(TestContext);
  const { tests, getAllTests, setPage } = testContext;

  useEffect(() => {
    getAllTests();

    // eslint-disable-next-line
  }, []);

  const [search, setSearch] = useState('');
  const onChange = e => {
    setSearch(e.target.value);
    setPage(1);
  };

  const filteredTests =
    tests.length !== 0 &&
    tests.filter(test => {
      return (
        test.test_name.toLowerCase().includes(search.toLowerCase()) ||
        test.facility.toLowerCase().includes(search.toLowerCase())
      );
    });

  return (
    <div className='container wrapper'>
      <div className='search-tests'>
        <input
          type='text'
          placeholder='Search Tests by Facility or Test Name ...'
          className='search-form search-test-input'
          onChange={onChange}
          value={search}
        />
      </div>
      <ShowTests tests={filteredTests} />
    </div>
  );
};

export default SearchTests;
