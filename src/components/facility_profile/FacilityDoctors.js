import React, { useContext, useEffect } from 'react';
import DoctorContext from '../../context/doctor/doctorContext';
import FacilityContext from '../../context/facility/facilityContext';

import history from '../../history';

const FacilityDoctors = () => {
  const doctorContext = useContext(DoctorContext);
  const facilityContext = useContext(FacilityContext);

  const { getDoctorsByFacility, allDoctors, getDoctorById } = doctorContext;

  const { getFacilityById, facility } = facilityContext;

  useEffect(() => {
    const facility_id = localStorage.getItem('facility_id');
    getFacilityById(facility_id);
    if (facility) {
      getDoctorsByFacility(facility._id);
    } else {
      getDoctorsByFacility(facility_id);
    }
    // eslint-disable-next-line
  }, []);

  const getDoctor = (doc_id, faci_id) => {
    localStorage.removeItem('doctor_id');
    localStorage.removeItem('facility_id');
    setTimeout(() => {
      history.push('/doctor');
    }, 100);
    getDoctorById(doc_id);
    getFacilityById(faci_id);
  };

  return (
    <div className='container wrapper'>
      <h1
        style={{
          margin: '20px 0 10px 0',
          color: 'grey',
          fontSize: '16px',
          fontWeight: '500',
          textTransform: 'uppercase',
          letterSpacing: '1.2px',
          textAlign: 'center',
        }}
      >
        <span
          style={{
            color: '#004579',
          }}
        >
          Doctors{' '}
        </span>{' '}
        | {facility && facility.facility_name}
      </h1>
      <div className='medical-professionals-section-content'>
        {allDoctors.map(doctor => (
          <div
            className='medical-professionals-section-individual-content doc-card'
            key={doctor._id}
            onClick={() => getDoctor(doctor._id, doctor.facility_id)}
          >
            <img src={doctor.doctor_image} alt='' />
            <div className='pv-10 ph-15'>
              <h4>{doctor.doctor_name}</h4>
              {doctor.doctor_speciality.map((speciality, index) => (
                <p key={index}>{(index ? ', ' : '') + speciality}</p>
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default FacilityDoctors;
