import React, { useEffect, useContext } from 'react';
import { LoadScript, GoogleMap, Marker } from '@react-google-maps/api';
import history from '../../history';

import DoctorContext from '../../context/doctor/doctorContext';
import CabinContext from '../../context/cabin/cabinContext';
import FacilityContext from '../../context/facility/facilityContext';

import { map_api } from '../../config';

const Profile = () => {
  const doctorContext = useContext(DoctorContext);
  const cabinContext = useContext(CabinContext);
  const facilityContext = useContext(FacilityContext);

  const { getDoctorsByFacility, allDoctors } = doctorContext;
  const { getCabinsByFacility } = cabinContext;
  const { getFacilityById, facility } = facilityContext;

  useEffect(() => {
    const facility_id = localStorage.getItem('facility_id');
    getFacilityById(facility_id);
    if (facility) {
      getDoctorsByFacility(facility._id);
      getCabinsByFacility(facility._id);
    } else {
      getDoctorsByFacility(facility_id);
      getCabinsByFacility(facility_id);
    }
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      <div
        style={{
          backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${facility &&
            facility.facility_image})`,
        }}
        className='facility-top'
      >
        <div className='facility-top-content'>
          <h1
            style={{
              textTransform: 'uppercase',
              letterSpacing: '1.2px',
              color: '#e2e2e2',
              fontWeight: '300',
              textShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.5)',
            }}
          >
            {facility && facility.facility_name}
          </h1>
          <button
            onClick={() => history.push('/showCabins')}
            className='book-cabin-btn'
          >
            Book Cabin
          </button>
        </div>
      </div>
      <div className='facility-middle container'>
        <div className='facility-middle-map'>
          <LoadScript id='script-loader' googleMapsApiKey={map_api}>
            <GoogleMap
              id='circle-example'
              mapContainerStyle={{
                height: '300px',
                width: '100%',
                borderRadius: '4px',
              }}
              zoom={10}
              center={{
                lat: facility && facility.location.coordinates[1],
                lng: facility && facility.location.coordinates[0],
              }}
            >
              <Marker
                position={{
                  lat: facility && facility.location.coordinates[1],
                  lng: facility && facility.location.coordinates[0],
                }}
              />
            </GoogleMap>
          </LoadScript>
        </div>
        <div className='facility-middle-info'>
          <h2
            style={{
              textAlign: 'center',
              textTransform: 'uppercase',
              letterSpacing: '1.1px',
              marginTop: '40px',
              marginBottom: '20px',
              color: 'grey',
            }}
          >
            Contact
          </h2>
          <div className='facility-middle-info-content'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              height='24'
              viewBox='0 0 24 24'
              width='24'
            >
              <path d='M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z' />
              <path d='M0 0h24v24H0z' fill='none' />
            </svg>

            <p style={{ marginLeft: '12px', color: '#333', fontSize: '16px' }}>
              {facility && facility.facility_email}
            </p>
          </div>
          <div className='facility-middle-info-content'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              height='24'
              viewBox='0 0 24 24'
              width='24'
            >
              <path d='M0 0h24v24H0z' fill='none' />
              <path d='M6.62 10.79c1.44 2.83 3.76 5.14 6.59 6.59l2.2-2.2c.27-.27.67-.36 1.02-.24 1.12.37 2.33.57 3.57.57.55 0 1 .45 1 1V20c0 .55-.45 1-1 1-9.39 0-17-7.61-17-17 0-.55.45-1 1-1h3.5c.55 0 1 .45 1 1 0 1.25.2 2.45.57 3.57.11.35.03.74-.25 1.02l-2.2 2.2z' />
            </svg>

            <p style={{ marginLeft: '12px', color: '#333', fontSize: '16px' }}>
              {facility && facility.facility_phone}
            </p>
          </div>
          <div className='facility-middle-info-content'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              height='24'
              viewBox='0 0 24 24'
              width='24'
            >
              <path d='M0 0h24v24H0V0z' fill='none' />
              <path d='M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zM7 9c0-2.76 2.24-5 5-5s5 2.24 5 5c0 2.88-2.88 7.19-5 9.88C9.92 16.21 7 11.85 7 9z' />
              <circle cx='12' cy='9' r='2.5' />
            </svg>

            <p style={{ marginLeft: '12px', color: '#333', fontSize: '16px' }}>
              {facility && facility.location.facility_address}
            </p>
          </div>
        </div>
      </div>

      {allDoctors.length !== 0 && (
        <div className='container'>
          <button
            style={{
              color: 'grey',
              cursor: 'pointer',
              display: 'flex',
              fontSize: '16px',
              fontWeight: '500',
              marginBottom: '20px',
              marginTop: '40px',
              borderStyle: 'none',
              padding: '5px',
              borderRadius: '4px',
            }}
            onClick={() => history.push('/facilityDoctors')}
          >
            Take a look at our medical team ...
          </button>
        </div>
      )}
    </div>
  );
};

export default Profile;
