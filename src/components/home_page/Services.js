import React from 'react';
import medical from '../../assets/medical.png';
import test from '../../assets/test.png';
import hospital from '../../assets/hospital.png';
import wellness from '../../assets/wellness.png';
import specialized from '../../assets/specialized.png';

const Services = () => {
  return (
    <div className='service-section'>
      <div className='heading container'>
        <h3>Our Services</h3>
      </div>
      <section className='container-2'>
        <div className='individual-content card'>
          <img src={medical} alt='' />
          <div className='pv-10 ph-15'>
            <h2>Appointment</h2>
            <p>
              Book an appointment with our specialist professionals from home
            </p>
          </div>
        </div>
        <div className='individual-content card'>
          <img src={test} alt='' />
          <div className='pv-10 ph-15'>
            <h2>Medical Tests</h2>

            <p>
              Compare medical test cost with your local hospitals and choose the
              right one
            </p>
          </div>
        </div>
        <div className='individual-content card'>
          <img src={hospital} alt='' />
          <div className='pv-10 ph-15'>
            <h2>Hospital</h2>
            <p>
              Check our registered hospital's bed capacity and cabin
              availability from our site
            </p>
          </div>
        </div>
        <div className='individual-content card'>
          <img src={wellness} alt='' />
          <div className='pv-10 ph-15'>
            <h2>Wellness</h2>
            <p>
              Wellness services promote and facilitate general health and
              wellness for patients
            </p>
          </div>
        </div>
        <div className='individual-content card'>
          <img src={specialized} alt='' />
          <div className='pv-10 ph-15'>
            <h2>Specialized Care</h2>
            <p>
              Specialized care includes mental health specialist, 24/7 in-house
              psych facilities.
            </p>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Services;
