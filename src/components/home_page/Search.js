import React, { useEffect, useContext, useState } from 'react';
import Select from 'react-select';
import DoctorContext from '../../context/doctor/doctorContext';
import FacilityContext from '../../context/facility/facilityContext';
import ShowDoctors from '../find_page/ShowDoctors';

const Search = () => {
  const doctorContext = useContext(DoctorContext);
  const facilityContext = useContext(FacilityContext);
  const {
    getAllDoctors,
    getDoctorsByFacility,
    getDoctorsBySpeciality,
    getDoctorsByName,
    getAllSpecialities,
    allSpecialities,
    getDoctorsByFacilityAndSpeciality,
    getDoctorsByFacilitySpecialityAndName,
  } = doctorContext;

  const { getAllFacilities, allFacilities } = facilityContext;
  useEffect(() => {
    getAllDoctors();
    getAllFacilities();
    getAllSpecialities();
    // eslint-disable-next-line
  }, []);
  const [searchActivation, setSearchActivation] = useState(false);
  const [facility, setFacility] = useState('');
  const [speciality, setSpeciality] = useState('');
  const [name, setName] = useState('');
  const onChange = facility => {
    setFacility(facility);
  };
  const onChange2 = speciality => {
    setSpeciality(speciality);
  };
  const onNameChange = e => {
    setName(e.target.value);
  };
  const facilities = allFacilities.map(facility => ({
    value: facility._id,
    label: facility.facility_name,
  }));

  const specialities = allSpecialities.map(speciality => ({
    value: speciality,
    label: speciality,
  }));

  const onSearch = () => {
    setSearchActivation(true);
    window.scrollTo(0, 600);

    if (facility !== '' && speciality === '' && name === '') {
      getDoctorsByFacility(facility.value);
      setFacility('');
    } else if (speciality !== '' && facility === '' && name === '') {
      getDoctorsBySpeciality(speciality.value);
      setSpeciality('');
    } else if (name !== '' && facility === '' && speciality === '') {
      getDoctorsByName(name);
      setName('');
    } else if (facility !== '' && speciality !== '' && name === '') {
      getDoctorsByFacilityAndSpeciality(facility.value, speciality.value);
      setFacility('');
      setSpeciality('');
    } else if (facility !== '' && speciality !== '' && name !== '') {
      getDoctorsByFacilitySpecialityAndName(
        facility.value,
        speciality.value,
        name,
      );
      setFacility('');
      setSpeciality('');
      setName('');
    }
  };

  const onKeyDown = e => {
    if (e.keyCode === 13) {
      onSearch();
    }
  };

  // custome styles for react-select
  const styles = {
    input: (provided, state) => ({
      ...provided,
      padding: '15px 0',
    }),
    indicatorSeparator: (provided, state) => ({
      ...provided,
      display: 'none',
    }),
  };

  return (
    <div>
      <div className='home-hero'>
        <section className='home-heading container'>
          <h1>Advanced Medical Subsidiary</h1>
          <p>Find best medical care at your nearest location.</p>
        </section>
        <section className='home-search container'>
          <Select
            onChange={onChange}
            value={facility}
            options={facilities}
            placeholder='Any Facility ...'
            styles={styles}
            onKeyDown={onKeyDown}
          />
          <Select
            onChange={onChange2}
            value={speciality}
            options={specialities}
            placeholder='Any Speciality ...'
            styles={styles}
            onKeyDown={onKeyDown}
          />

          <input
            className='search-form'
            type='text'
            value={name}
            onChange={onNameChange}
            placeholder='Doctor name (Optional)'
            onKeyDown={onKeyDown}
          />
          <button onClick={onSearch} className='btn btn-search'>
            <i style={{ fontSize: '18px' }} className='material-icons'>
              search
            </i>
          </button>
        </section>
      </div>
      {searchActivation && <ShowDoctors fromNav={false} />}
    </div>
  );
};

export default Search;
