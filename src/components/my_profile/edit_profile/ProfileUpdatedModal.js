import React from 'react';
import history from '../../../history';

const ProfileUpdatedModal = () => {
  return (
    <div className='backdrop'>
      <div className='negative-alert'>
        <p className='mv-10'>Your Profile has been updated successfully.</p>
        <button onClick={() => history.push('/profile')}>
          Return to Profile
        </button>
      </div>
    </div>
  );
};

export default ProfileUpdatedModal;
