import React, { useContext, useEffect, useState } from 'react';
import { LoadScript, Autocomplete } from '@react-google-maps/api';
import SideNav from '../../global/SideNav';
import Navbar from '../../global/Navbar';
import AuthContext from '../../../context/auth/authContext';

import ProfileUpdatedModal from './ProfileUpdatedModal';

import { url, preset_patient, formData, map_api } from '../../../config';

const places = ['places'];

const EditForm = () => {
  const [isUpdated, setIsUpdated] = useState(false);
  const authContext = useContext(AuthContext);
  const {
    load_patient,
    patient,
    createProfile,
    updateProfile,
    profile,
    getProfile,
  } = authContext;
  useEffect(() => {
    load_patient();
    if (patient) {
      getProfile(patient._id);
    }
    // eslint-disable-next-line
  }, []);
  const [patient_data, setPatient_data] = useState({
    name: '',
    phone: '',
  });
  const { name, phone } = patient_data;
  const [address, setAddress] = useState('');
  const [address_name, setAddress_name] = useState('');
  const onLoad = address => {
    setAddress(address);
  };
  const onPlaceChanged = () => {
    if (address !== '') {
      setAddress_name(address.getPlace().formatted_address);
    } else {
      alert('address is not loaded');
    }
  };
  const onChange = e => {
    setPatient_data({ ...patient_data, [e.target.name]: e.target.value });
  };
  const [pic, setPic] = useState('');
  const onPicChange = e => {
    setPic(e.target.files[0]);
  };
  const onSubmit = async () => {
    formData.append('file', pic);
    formData.append('upload_preset', preset_patient);

    try {
      let image = null;

      const res = await fetch(url, {
        method: 'POST',
        body: formData,
      });
      image = await res.json();

      const data = {
        name: name,
        phone: phone,
        address: address_name,
        pic: image === null ? profile.patient_pic : image.secure_url,
      };
      if (profile !== null) {
        updateProfile(patient._id, data);
        setIsUpdated(true);
      } else {
        createProfile(patient._id, data);
        setIsUpdated(true);
      }
    } catch (err) {
      console.error('OOPS!!', err);
    }
  };

  if (isUpdated) return <ProfileUpdatedModal />;
  return (
    <>
      <SideNav />
      <Navbar />
      <div className='container'>
        <div className='input-form-wrapper'>
          <h2
            style={{
              fontSize: '20px',
              textTransform: 'uppercase',
              letterSpacing: '1.1px',
              color: 'grey',
              textAlign: 'center',
            }}
          >
            Update Profile
          </h2>
          <input
            type='text'
            name='name'
            onChange={onChange}
            value={name}
            placeholder='Your Name'
            className='input-form'
          />
          <input
            type='text'
            placeholder='Your Phone Number'
            className='input-form'
            name='phone'
            onChange={onChange}
            value={phone}
          />
          <LoadScript googleMapsApiKey={map_api} libraries={places}>
            <Autocomplete onPlaceChanged={onPlaceChanged} onLoad={onLoad}>
              <input
                type='text'
                placeholder='Your Address'
                className='input-form'
                name='address'
                style={{ marginBottom: '20px' }}
              />
            </Autocomplete>
          </LoadScript>

          <input type='file' onChange={onPicChange} id='profile-pic' />
          {pic ? (
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                background: '#f0f0f0',
                padding: '5px',
              }}
            >
              <svg
                xmlns='http://www.w3.org/2000/svg'
                height='20'
                viewBox='0 0 24 24'
                width='20'
                fill='#555'
              >
                <path d='M0 0h24v24H0V0z' fill='none' />
                <path d='M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z' />
              </svg>
              <span style={{ fontSize: '14px', marginLeft: '5px' }}>
                {pic.name}
              </span>
            </div>
          ) : (
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <svg
                xmlns='http://www.w3.org/2000/svg'
                height='24'
                viewBox='0 0 24 24'
                width='24'
                fill='#555'
              >
                <path d='M0 0h24v24H0V0z' fill='none' />
                <path d='M18 20H4V6h9V4H4c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-9h-2v9zm-7.79-3.17l-1.96-2.36L5.5 18h11l-3.54-4.71zM20 4V1h-2v3h-3c.01.01 0 2 0 2h3v2.99c.01.01 2 0 2 0V6h3V4h-3z' />
              </svg>
              <label
                style={{
                  textTransform: 'uppercase',
                  letterSpacing: '1.1px',
                  fontWeight: '500',
                  color: '#555',
                  cursor: 'pointer',
                  borderBottom: '1px solid grey',
                  marginLeft: '10px',
                }}
                htmlFor='profile-pic'
              >
                Upload an Image
              </label>
            </div>
          )}

          <button
            style={{ marginTop: '20px' }}
            onClick={onSubmit}
            className='input-form submit'
          >
            Submit
          </button>
        </div>
      </div>
      ;
    </>
  );
};

export default EditForm;
