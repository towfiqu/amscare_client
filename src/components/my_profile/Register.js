import React, { useState, useContext } from 'react';
import history from '../../history';
import axios from 'axios';
import AppContext from '../../context/app/appContext';

import { Link } from 'react-router-dom';

import { getApiString } from '../../config';
const api_string = getApiString();

const Register = () => {
  const appContext = useContext(AppContext);
  const { alert, setAlert } = appContext;
  const [registerData, setRegisterData] = useState({
    email: '',
    password: '',
    confirm_password: '',
  });

  const { email, password, confirm_password } = registerData;

  const onChange = e => {
    setRegisterData({ ...registerData, [e.target.name]: e.target.value });
  };

  const onSubmit = async () => {
    try {
      const data = {
        email: email,
        password: password,
      };
      if (password !== confirm_password) {
        setAlert('passwords do not match!');
      } else {
        const res = await axios.post(
          `${api_string}/api/patients/register`,
          data,
        );
        if (res) {
          history.push('/login');
        }
      }
    } catch (err) {
      console.error('OOOPS!!!', err);
    }
  };

  return (
    <div className='container wrapper'>
      {alert && <h2 className='alert'>{alert}</h2>}
      <div className='input-form-wrapper'>
        <h2
          style={{
            fontSize: '20px',
            textTransform: 'uppercase',
            letterSpacing: '1.1px',
            color: 'grey',
            textAlign: 'center',
          }}
        >
          Register
        </h2>

        <input
          className='input-form'
          type='email'
          name='email'
          onChange={onChange}
          value={email}
          placeholder='email@example.com'
        />

        <input
          className='input-form'
          type='password'
          name='password'
          onChange={onChange}
          value={password}
          placeholder='password'
        />
        <input
          className='input-form'
          type='password'
          name='confirm_password'
          onChange={onChange}
          value={confirm_password}
          placeholder='Confirm password'
        />
        <button onClick={onSubmit} className='input-form submit'>
          Submit
        </button>
        <p style={{ marginTop: '15px', textAlign: 'center' }}>
          To Login{' '}
          <Link to='/login' alt=''>
            click here
          </Link>
        </p>
      </div>
    </div>
  );
};

export default Register;
