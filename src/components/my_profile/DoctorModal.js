import React, { useRef, useEffect, useContext, useState } from 'react';
import AppContext from '../../context/app/appContext';
import AuthContext from '../../context/auth/authContext';

import ReportSent from './ReportSent';

import axios from 'axios';

import { getApiString } from '../../config';
const api_string = getApiString();

const DoctorModal = ({ allDoctors }) => {
  const appContext = useContext(AppContext);
  const authContext = useContext(AuthContext);

  const [confirmSent, setConfirmSent] = useState(false);

  const { profile, patient_report } = authContext;

  const { setModalTrigger } = appContext;
  const node = useRef();
  useEffect(() => {
    document.addEventListener('click', handleClick);
    return () => {
      document.removeEventListener('click', handleClick);
    };
    // eslint-disable-next-line
  }, []);
  const handleClick = e => {
    if (node.current.contains(e.target)) {
      return;
    }
    setModalTrigger(false);
  };

  const [searchText, setSearchText] = useState('');
  const onSearch = e => {
    setSearchText(e.target.value);
    // console.log(searchText);
  };

  const filteredDoc = allDoctors.filter(doctor => {
    return doctor.doctor_name.toLowerCase().includes(searchText.toLowerCase());
  });

  const onSelectDoc = async doctor => {
    const data = {
      doc_email: doctor.doctor_email,
      patient_email: profile.patient_email,
      patient_name: profile.patient_name,
      patient_phone: profile.patient_phone,
      report: patient_report,
    };

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const res = await axios.post(
      `${api_string}/api/patients/send_report`,
      config,
      { data },
    );
    console.log(res.data);

    if (res.data.msg === 'report sent') {
      // setModalTrigger(false);
      setConfirmSent(true);
    }
  };

  if (confirmSent) {
    setTimeout(() => {
      setModalTrigger(false);
    }, 2000);
    return <ReportSent />;
  }

  return (
    <div className='backdrop'>
      <div className='modal' ref={node}>
        <h4
          style={{
            textAlign: 'center',
            color: 'grey',
            textTransform: 'uppercase',
            margin: '20px auto auto auto',
            letterSpacing: '1px',
          }}
        >
          Send report to your physician.
        </h4>
        <input
          className='doc-search'
          placeholder='Search Doctor'
          type='text'
          name='doc_search'
          onChange={onSearch}
          value={searchText}
        />
        {filteredDoc.map(doctor => (
          <div
            onClick={() => onSelectDoc(doctor)}
            key={doctor._id}
            className='doctor-card'
          >
            <img
              style={{
                width: '50px',
                height: '50px',
                borderRadius: '50%',
                justifySelf: 'center',
              }}
              src={doctor.doctor_image}
              alt=''
            />
            <div className='doctor-info'>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <h4 style={{ color: '#004579' }}>{doctor.doctor_name}</h4>
                <p style={{ fontSize: '14px', marginLeft: '10px' }}>
                  {doctor.doctor_speciality}
                </p>
              </div>
              <p>{doctor.doctor_email}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default DoctorModal;
