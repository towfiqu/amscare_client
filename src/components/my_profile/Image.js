import React from 'react';
const Image = ({ image }) => {
  return (
    <div className='full-image container'>
      <span onClick={() => window.location.reload()} className='close'>
        Close X
      </span>
      <img src={image} alt='' />
    </div>
  );
};

export default Image;
