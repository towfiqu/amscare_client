import React, { useContext, useEffect, useState } from 'react';
import history from '../../history';
import AuthContext from '../../context/auth/authContext';
import DoctorContext from '../../context/doctor/doctorContext';
import AppContext from '../../context/app/appContext';
import SideNav from '../global/SideNav';
import Navbar from '../global/Navbar';
import Footer from '../global/Footer';
import axios from 'axios';
import Image from './Image';
import NoProfile from './NoProfile';
import DoctorModal from './DoctorModal';

import { url, preset_report, formData } from '../../config';

const Profile = () => {
  const doctorContext = useContext(DoctorContext);
  const { getAllDoctors, allDoctors } = doctorContext;

  const authContext = useContext(AuthContext);
  const { profile, getProfile, addReport, setPatientReport } = authContext;

  const appContext = useContext(AppContext);
  const { setModalTrigger, modalTrigger } = appContext;

  const [show, setShow] = useState(false);
  const [image, setImage] = useState('');
  const [report, setReport] = useState('');

  const onImageClick = image => {
    setShow(true);
    setImage(image);
  };

  useEffect(() => {
    getAllDoctors();
    const patient_id = localStorage.getItem('patient_id');
    if (patient_id) {
      getProfile(patient_id);
    }
    // eslint-disable-next-line
  }, []);

  if (show) return <Image image={image} />; /* will show full size report */
  if (profile === null || profile === undefined) return <NoProfile />;

  // adding report

  const onChange = e => {
    setReport(e.target.files[0]);
  };
  const onAddReport = async () => {
    formData.append('file', report);
    formData.append('upload_preset', preset_report);
    try {
      const res = await fetch(url, {
        method: 'POST',
        body: formData,
      });
      const report = await res.json();
      addReport(profile.patient_register_id, report.secure_url);
    } catch (err) {
      console.error('OOOPS!!', err);
    }
  };

  const onRemove = async image => {
    try {
      const res = await axios.post(
        `/api/patients/delete_report/${profile.patient_register_id}`,
        { image },
      );
      if (res) {
        window.location.reload();
      }
    } catch (err) {
      console.error('OOPS!!', err);
    }
  };

  const sendToDoc = image => {
    setModalTrigger(true);
    setPatientReport(image);
  };

  return (
    <>
      <SideNav />
      <Navbar />
      <div className='container wrapper'>
        {modalTrigger && <DoctorModal allDoctors={allDoctors} />}
        <h4
          className='patient-profile-title'
          style={{
            textTransform: 'uppercase',
            letterSpacing: '1.1px',
            fontWeight: '500',
            color: 'grey',
          }}
        >
          Patient Profile
        </h4>
        <div className='patient-card'>
          <img
            style={{
              width: '100px',
              height: '100px',
              borderRadius: '50%',
              justifySelf: 'center',
            }}
            src={profile && profile.patient_pic}
            alt=''
          />
          <div className='patient-info'>
            <p style={{ color: '#004579', fontWeight: '700' }}>
              {' '}
              {profile && profile.patient_name}
            </p>
            <p>{profile && profile.patient_email}</p>
            <p>{profile && profile.patient_phone}</p>
            <p>{profile && profile.patient_address}</p>
          </div>
          <svg
            onClick={() => history.push('/edit_profile')}
            className='profile-edit-icon'
            xmlns='http://www.w3.org/2000/svg'
            height='24'
            viewBox='0 0 24 24'
            width='24'
          >
            <path d='M0 0h24v24H0V0z' fill='none' />
            <path d='M14.06 9.02l.92.92L5.92 19H5v-.92l9.06-9.06M17.66 3c-.25 0-.51.1-.7.29l-1.83 1.83 3.75 3.75 1.83-1.83c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.2-.2-.45-.29-.71-.29zm-3.6 3.19L3 17.25V21h3.75L17.81 9.94l-3.75-3.75z' />
          </svg>
        </div>
        <div className='upload-report mv-20'>
          <h4
            style={{
              textTransform: 'uppercase',
              letterSpacing: '1.1px',
              fontWeight: '500',
              color: 'grey',
              alignSelf: 'center',
            }}
          >
            Upload Reports
          </h4>
          <div>
            {report && (
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  background: '#f0f0f0',
                  padding: '5px',
                }}
              >
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  height='20'
                  viewBox='0 0 24 24'
                  width='20'
                  fill='grey'
                >
                  <path d='M0 0h24v24H0V0z' fill='none' />
                  <path d='M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z' />
                </svg>
                <span style={{ fontSize: '14px', marginLeft: '5px' }}>
                  {report.name}
                </span>
              </div>
            )}
          </div>
          {report ? (
            <button
              style={{
                borderRadius: '4px',
                color: 'white',
                cursor: 'pointer',
                borderStyle: 'none',
                background: '#004579',
                boxShadow: '0 2px 4px 0 rgba(0,0,0,0.3)',
                padding: '5px',
                width: '90%',
                justifySelf: 'right',
              }}
              onClick={onAddReport}
            >
              Save Report
            </button>
          ) : (
            <div
              style={{
                display: 'flex',
                justifySelf: 'right',
              }}
            >
              <input type='file' id='upload-report' onChange={onChange} />
              <svg
                xmlns='http://www.w3.org/2000/svg'
                height='20'
                viewBox='0 0 24 24'
                width='20'
                fill='grey'
              >
                <path d='M0 0h24v24H0V0z' fill='none' />
                <path d='M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z' />
              </svg>
              <label
                style={{
                  textTransform: 'uppercase',
                  letterSpacing: '1.1px',
                  fontWeight: '500',
                  color: '#555',
                  cursor: 'pointer',
                  borderBottom: '1px solid grey',
                }}
                htmlFor='upload-report'
              >
                ADD REPORT
              </label>
            </div>
          )}
        </div>
        <div className='reports-container'>
          {profile.patient_reports.length > 0 ? (
            profile.patient_reports.map(image => (
              <div key={image} className='hover-container'>
                <img className='report' src={image} alt='' />
                <div className='overlay'>
                  <button
                    onClick={() => onImageClick(image)}
                    className='open-btn'
                  >
                    Open
                  </button>
                  <button onClick={() => sendToDoc(image)} className='send-btn'>
                    Send To Doctor
                  </button>
                  <button
                    className='delete-btn'
                    onClick={() => onRemove(image)}
                  >
                    Delete
                  </button>
                </div>
              </div>
            ))
          ) : (
            <h2 className='no-report-alert'>You have no reports</h2>
          )}{' '}
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Profile;
