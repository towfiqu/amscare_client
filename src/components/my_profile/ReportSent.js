import React from 'react';

const ReportSent = () => {
  return (
    <div className='backdrop'>
      <div className='negative-alert'>
        <p>Your Report has been sent.</p>
      </div>
    </div>
  );
};

export default ReportSent;
