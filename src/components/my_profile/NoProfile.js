import React, { useContext, useEffect } from 'react';
import history from '../../history';
import SideNav from '../global/SideNav';
import Navbar from '../global/Navbar';
import AuthContext from '../../context/auth/authContext';

const NoProfile = () => {
  const authContext = useContext(AuthContext);
  const { load_patient } = authContext;
  useEffect(() => {
    const patient_id = localStorage.getItem('patient_id');
    if (patient_id) {
      load_patient();
    }
    // eslint-disable-next-line
  }, []);
  return (
    <>
      <SideNav />
      <Navbar />
      <div className='container'>
        <div className='no-profile'>
          <h2>Your Profile is not complete yet.</h2>
          <button
            onClick={() => history.push('/edit_profile')}
            className='complete-profile'
          >
            Complete Profile
          </button>
        </div>
      </div>
    </>
  );
};

export default NoProfile;
