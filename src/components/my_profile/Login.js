import React, { useState, useContext, useEffect } from 'react';
import history from '../../history';
import AuthContext from '../../context/auth/authContext';

import { Link } from 'react-router-dom';

const Login = () => {
  const authContext = useContext(AuthContext);
  const { login, isAuthenticated } = authContext;
  const [registerData, setRegisterData] = useState({
    email: '',
    password: '',
  });

  useEffect(() => {
    if (isAuthenticated) {
      setTimeout(() => {
        history.push('/profile');
      }, 300);
    }
  }, [isAuthenticated]);

  const { email, password } = registerData;

  const onChange = e => {
    setRegisterData({ ...registerData, [e.target.name]: e.target.value });
  };

  const onSubmit = async () => {
    try {
      const data = {
        email: email,
        password: password,
      };

      login(data);
    } catch (err) {
      console.error('OOOPS!!!', err);
    }
  };

  return (
    <div className='container wrapper'>
      <div className='input-form-wrapper'>
        <h2
          style={{
            fontSize: '20px',
            textTransform: 'uppercase',
            letterSpacing: '1.1px',
            color: 'grey',
            textAlign: 'center',
          }}
        >
          Login
        </h2>

        <input
          className='input-form'
          type='email'
          name='email'
          onChange={onChange}
          value={email}
          placeholder='email@example.com'
        />

        <input
          className='input-form'
          type='password'
          name='password'
          onChange={onChange}
          value={password}
          placeholder='password'
        />

        <button onClick={onSubmit} className='input-form submit'>
          Submit
        </button>
        <p style={{ marginTop: '15px', textAlign: 'center' }}>
          To Register{' '}
          <Link to='/register' alt=''>
            click here
          </Link>
        </p>
      </div>
    </div>
  );
};

export default Login;
