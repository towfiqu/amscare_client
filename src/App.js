import React from 'react';
import history from './history';
import { Router, Route, Switch } from 'react-router-dom';
import './App.scss';

import setAuthToken from './utils/setAuthToken';

import Home from './pages/Home';
import Find from './pages/Find';
import Apt from './pages/appointment/Apt';
import Facilities from './pages/Facilities';
import ConfirmApt from './pages/appointment/ConfirmApt';
import Doctor from './pages/profiles/Doctor';
import AppState from './context/app/AppState';
import TestState from './context/test/TestState';
import DoctorState from './context/doctor/DoctorState';
import FacilityState from './context/facility/FacilityState';
import CabinState from './context/cabin/CabinState';
import AuthState from './context/auth/AuthState';
import Facility from './pages/profiles/Facility';
import ShowCabins from './pages/cabins/ShowCabins';
import CabinForm from './pages/cabins/CabinForm';
import CabinDetails from './pages/cabins/CabinDetails';
import Medical_tests from './pages/Medical_tests';
import Register from './pages/my_profile/Register';
import Login from './pages/my_profile/Login';
import Profile from './components/my_profile/Profile';
import EditProfile from './components/my_profile/edit_profile/EditForm';
import ConfirmPayment from './components/cabins/ConfirmPayment';
import NearestFacilities from './pages/NearestFacilities';
import ShowFacilittDoctors from './pages/ShowFacilityDoctors';

import { getApiString } from './config';

if (localStorage.token) {
  setAuthToken(localStorage.token);
}

const App = () => {
  console.log(getApiString());
  console.log(process.env.NODE_ENV);
  return (
    <AppState>
      <DoctorState>
        <FacilityState>
          <CabinState>
            <TestState>
              <AuthState>
                <Router history={history}>
                  <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/find' component={Find} />
                    <Route exact path='/doctor' component={Doctor} />
                    <Route exact path='/facility' component={Facility} />
                    <Route exact path='/apt' component={Apt} />
                    <Route exact path='/confirm_apt' component={ConfirmApt} />
                    <Route exact path='/facilities' component={Facilities} />
                    <Route exact path='/showCabins' component={ShowCabins} />
                    <Route exact path='/cabinForm' component={CabinForm} />
                    <Route
                      exact
                      path='/facilityDoctors'
                      component={ShowFacilittDoctors}
                    />
                    <Route
                      exact
                      path='/cabin_details'
                      component={CabinDetails}
                    />
                    <Route
                      exact
                      path='/confirmPayment'
                      component={ConfirmPayment}
                    />
                    <Route
                      exact
                      path='/medical_tests'
                      component={Medical_tests}
                    />
                    <Route exact path='/register' component={Register} />
                    <Route exact path='/login' component={Login} />
                    <Route exact path='/profile' component={Profile} />
                    <Route exact path='/edit_profile' component={EditProfile} />
                    <Route
                      exact
                      path='/nearest_facilities'
                      component={NearestFacilities}
                    />
                  </Switch>
                </Router>
              </AuthState>
            </TestState>
          </CabinState>
        </FacilityState>
      </DoctorState>
    </AppState>
  );
};

export default App;
