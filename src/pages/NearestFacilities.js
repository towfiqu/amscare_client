import React from 'react';
import LocationInput from '../components/nearest_facilities/LocationInput';

const NearestFacilities = () => {
  return (
    <div>
      <LocationInput />
    </div>
  );
};

export default NearestFacilities;
