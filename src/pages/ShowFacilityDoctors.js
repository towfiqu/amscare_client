import React from 'react';
import FacilityDoctors from '../components/facility_profile/FacilityDoctors';
import Navbar from '../components/global/Navbar';
import SideNav from '../components/global/SideNav';
import Footer from '../components/global/Footer';

const ShowFacilityDoctors = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <FacilityDoctors />
      <Footer />
    </div>
  );
};

export default ShowFacilityDoctors;
