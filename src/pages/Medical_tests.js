import React from 'react';
import Navbar from '../components/global/Navbar';
import SideNav from '../components/global/SideNav';
import Footer from '../components/global/Footer';
import SearchTests from '../components/medical_tests/SearchTests';

const Medical_tests = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <SearchTests />
      <Footer />
    </div>
  );
};

export default Medical_tests;
