import React from 'react';
import Navbar from '../components/global/Navbar';
import Footer from '../components/global/Footer';
import Search from '../components/find_page/Search';
import ShowDoctors from '../components/find_page/ShowDoctors';
import SideNav from '../components/global/SideNav';

const Find = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <Search />
      <ShowDoctors fromNav={true} />
      <Footer />
    </div>
  );
};

export default Find;
