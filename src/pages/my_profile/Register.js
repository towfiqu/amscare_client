import React from 'react';
import SideNav from '../../components/global/SideNav';
import Navbar from '../../components/global/Navbar';
import Footer from '../../components/global/Footer';
import RegisterComponent from '../../components/my_profile/Register';

const Register = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <RegisterComponent />
      <Footer />
    </div>
  );
};

export default Register;
