import React from 'react';
import SideNav from '../../components/global/SideNav';
import Navbar from '../../components/global/Navbar';
import Footer from '../../components/global/Footer';
import LoginComponent from '../../components/my_profile/Login';

const Register = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <LoginComponent />
      <Footer />
    </div>
  );
};

export default Register;
