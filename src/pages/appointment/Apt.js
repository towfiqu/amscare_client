import React from 'react';
import SideNav from '../../components/global/SideNav';
import Navbar from '../../components/global/Navbar';
import Footer from '../../components/global/Footer';
import AptForm from '../../components/appointment/AptForm';

const Apt = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <AptForm />
      <Footer />
    </div>
  );
};

export default Apt;
