import React from 'react';
import SideNav from '../../components/global/SideNav';
import Navbar from '../../components/global/Navbar';
import Footer from '../../components/global/Footer';
import BookingForm from '../../components/cabins/BookingForm';

const CabinForm = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <BookingForm />
      <Footer />
    </div>
  );
};

export default CabinForm;
