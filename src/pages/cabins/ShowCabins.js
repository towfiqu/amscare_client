import React from 'react';
import SideNav from '../../components/global/SideNav';
import Navbar from '../../components/global/Navbar';
import Footer from '../../components/global/Footer';
import AllCabins from '../../components/cabins/AllCabins';

const ShowCabins = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <AllCabins />
      <Footer />
    </div>
  );
};

export default ShowCabins;
