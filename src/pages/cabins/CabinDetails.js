import React from 'react';
import BookingDetails from '../../components/cabins/BookingDetails';

const CabinDetails = () => {
  return (
    <div>
      <BookingDetails />
    </div>
  );
};

export default CabinDetails;
