import React from 'react';
import Navbar from '../components/global/Navbar';
import Footer from '../components/global/Footer';
import Search from '../components/home_page/Search';
import Services from '../components/home_page/Services';
import SideNav from '../components/global/SideNav';

const Home = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <Search />
      <Services />
      <Footer />
    </div>
  );
};

export default Home;
