import React from 'react';
import SideNav from '../components/global/SideNav';
import Navbar from '../components/global/Navbar';
import Footer from '../components/global/Footer';
import SearchFacilities from '../components/facilities/SearchFacilities';

const Facilities = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <SearchFacilities />
      <Footer />
    </div>
  );
};

export default Facilities;
