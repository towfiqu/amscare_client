import React from 'react';
import SideNav from '../../components/global/SideNav';
import Navbar from '../../components/global/Navbar';
import Footer from '../../components/global/Footer';
import Profile from '../../components/facility_profile/Profile';

const Facility = () => {
  return (
    <div>
      <SideNav />
      <Navbar />
      <Profile />
      <Footer />
    </div>
  );
};

export default Facility;
